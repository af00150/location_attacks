import matplotlib.pyplot as plt
import networkx as nx
import warnings
warnings.filterwarnings("ignore")

def plot(graph):
    print("\nGENERATED GRAPH:")
    pos = nx.spring_layout(graph)
    nx.draw(graph,pos,edge_color='blue',width=1,linewidths=2,\
    labels={node:node for node in graph.nodes()})
    nx.draw_networkx_edge_labels(graph,pos, edge_labels= nx.get_edge_attributes(graph,'weight'))

def plot_seg(graph):
    print("\nGENERATED GRAPH:")
    pos = nx.spring_layout(graph)

    #nodes 
    nx.draw_networkx_nodes(graph,pos,nodelist=graph.nodelist,node_color='r')
    
    #edges
    nx.draw_networkx_edges(graph,pos,edgelist=graph.edgelist, edge_color='b')
    nx.draw_networkx_edge_labels(graph,pos, edge_labels= nx.get_edge_attributes(graph,'weight'))

def plot_network_layout(node_points):
    label = [row[0] for row in node_points]
    x = [row[1] for row in node_points]
    y = [row[2] for row in node_points]
    fig, ax = plt.subplots()
    ax.scatter(x,y)
    for i, txt in enumerate(label):
        ax.annotate(txt, (x[i], y[i]))