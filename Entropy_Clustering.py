import networkx as nx
import numpy as np
import Clustering as clust
import Graph_Reducer as gr
import Graph_Analyser as ga
import Plotting as plot

from copy import copy, deepcopy

def Cluster(edm, growth_rate, optimal_cluster_size):
    #Generate the EDMs graph
    graph = ga.gen_sparse_graph(edm=edm)
    #Generate reduced graph with entropy matrix
    reduced_graph, entropy_matrix = gr.Reduce_Graph(edm=edm,graph=graph)
    
    #Get edges of the original graph
    graph_edges = list(graph.edges.data('weight'))
    
    #Plot original Graph from EDM
    print("Original Graph")
    plot.plot(graph)
    #Plot Reduced Graph from Graph_Reducer
    print("Reduced Graph")
    plot.plot(reduced_graph)
    
    #Get the max graph edge length
    max_edge = sorted(graph.edges.data('weight'), key=lambda x:x[2])[len(graph.edges)-1][2]
    
    #remove duplicate entropy incriments (MinNorm)
    entropy_matrix = entropy_matrix / min(entropy_matrix)
    
    #Normalise to (MaxNorm)
    entropy_matrix = entropy_matrix / max(entropy_matrix)
    
    #Print initial Entropy Matrix after normalization
    print("Min&Max Normalised Entropy Matrix")
    
    #Deep copy of the Entropy_matrix
    entropy_matrix_test = deepcopy(entropy_matrix)
    #copy of the original graph
    graph_test = graph
    
    iteration = 1
    min_entropy = 0
    
    #Loop until all nodes have an entropy equal to that of the Max(edges)
    while min_entropy < max_edge:
        #iterate the node in the graph
        print("Iteration:",iteration)
        #node is the currently selected node
        for node in range(len(entropy_matrix_test)):
            #entropy_mnatrix[node] is the entropy value of that node
            node_entropy = entropy_matrix_test[node]
            #print("Node:", node , "Entropy:",node_entropy)
            if node_entropy > max_edge:
                continue
            #local_cluster will store the clusters for this itteration
            local_cluster = []
            #get all other nodes that reside within the radius of the nodes entropy
            for edge in range(len(graph_edges)):
                if graph_edges[edge][0] == node:
                    if graph_edges[edge][2] <= node_entropy:
                        local_cluster.append(np.array([graph_edges[edge][0],graph_edges[edge][1],graph_edges[edge][2]]))
                        print("Distance from Node:", node, "To Node:",graph_edges[edge][1], "is <=",node_entropy)
            if len(local_cluster) > 0:
                clusters.append(np.array(local_cluster))        

        #increase entropy of the node by the growth rate
        #entropy_matrix_test *= 1+growth_rate
        entropy_matrix_test += growth_rate
        min_entropy = min(entropy_matrix_test)
        print(entropy_matrix_test)
        #print(min_entropy)
        iteration+=1
        
    #Calculates the nodes that don't belong to clusters of a minimum size
    clust.check_intersection_instances(edm,clusters,optimal_cluster_size)
    
    #Init Group clusters    
    grouped_clusters = init_list_of_objects(len(entropy_matrix))    
    
    #Group the clusters for easy access
    for cluster in range(len(clusters)):
        print(len(clusters[cluster]),clusters[cluster])
        cluster_size = len(clusters[cluster])
        grouped_clusters[cluster_size-1].append(clusters[cluster])
    
    unique_grouped_clusters = init_list_of_objects(len(grouped_clusters))
    #Get Unique entries to each group
    for group in range(len(grouped_clusters)):
        if len(grouped_clusters[group]) > 0:
            unique_grouped_clusters[group] = np.unique(grouped_clusters[group],axis=0)
    
    #print cluster groups
    for cl in range(len(unique_grouped_clusters)):
        print("Cluster Size:", cl , "Clusters:", len(unique_grouped_clusters[cl]))
    
    return unique_grouped_clusters
    
    
    
def init_list_of_objects(size):
    list_of_objects = list()
    for i in range(0,size):
        list_of_objects.append( list() ) #different object reference each time
    return list_of_objects