import numpy as np
import math
import Graph_Analyser as ga
import random
import warnings
warnings.filterwarnings("ignore")

def gen_EDM():
    inifnity = 99
    EDM = np.array([
        [inifnity,6,7,5,6,9],
        [6,inifnity,2,4,8,8],
        [7,2,inifnity,3,5,4],
        [5,4,3,inifnity,3,3],
        [6,8,5,3,inifnity,4],
        [9,8,4,3,4,inifnity],
    ])
    print("COMPOSED EDM:")
    print(EDM)
    return EDM

def gen_larger_EDM():
    infinity = 99
    EDM = np.array([
        [infinity,2,8,7,3,8,4,6,12,5],
        [2,infinity,6,7,10,4,9,3,5,2],
        [8,6,infinity,1,4,8,5,2,2,9],
        [7,7,1,infinity,7,8,1,9,3,2],
        [3,10,4,7,infinity,5,9,14,10,3],
        [8,4,8,8,5,infinity,4,3,6,9],
        [4,9,5,1,9,4,infinity,8,8,2],
        [6,3,2,9,14,3,8,infinity,4,7],
        [12,5,2,3,10,6,8,4,infinity,5],
        [5,2,9,2,3,9,2,7,5,infinity]
    ])
    print("COMPOSED EDM:")
    print(EDM)
    return EDM

def generate_random_edm(shape,infinity_value,max_value, min_value):
    init_matrix = np.random.random_integers(min_value,max_value,size=shape)
    symmetrical_matrix = (init_matrix + init_matrix.T)/2
    np.fill_diagonal(symmetrical_matrix,infinity_value)
    print("COMPOSED EDM:")
    print(symmetrical_matrix)
    return symmetrical_matrix

def generate_random_sparse_edm(shape,infinity_value,max_value,min_value,sparsity):
    #edm = generate_random_edm(shape,infinity_value,max_value, min_value)
    init_matrix = np.random.random_integers(min_value,max_value,size=shape)
    edm = (init_matrix + init_matrix.T)/2
    np.fill_diagonal(edm,infinity_value)
    edm_mod = edm
    for i in range(int(edm.shape[0]*edm.shape[0]*sparsity)):
        x = random.randint(0,edm.shape[0]-1)
        y = random.randint(0,edm.shape[0]-1)
        edm_mod[x,y] = 99
        edm_mod[y,x] = 99
        if not ga.is_connected_graph(ga.gen_graph(edm)):
            i = i - 1
            edm_mod[x,y] = edm[x,y]
            edm_mod[y,x] = edm[y,x]
    print("COMPOSED SPARSE EDM:")
    print(edm_mod)
    return edm_mod

def eclid_calc_EDM(node_points, infinity ,edm):
    arr = np.zeros((len(node_points),len(node_points)))
    for x in range(len(node_points)):
        for y in range(len(node_points)):
            if edm[x][y] == infinity:
                arr[x][y] = infinity
            else:
                arr[x][y]= euclid_func(node_points[x][1],node_points[x][0],node_points[y][1],node_points[y][0])
    np.fill_diagonal(arr,infinity)
    return arr

def euclid_func(x1,y1,x2,y2):
    return math.sqrt((x2-x1)**2 + (y2-y1)**2)

def remove_edge(edm, source, destination, infinity):
    edm[source][destination] = infinity
    edm[destination][source] = infinity
    return edm

def remove_node(node_index, edm, infinity):
    for i in range(len(edm)):
        edm[node_index][i] = infinity
        edm[i][node_index] = infinity 
    return edm
    
    
    
    
    
    
    
    