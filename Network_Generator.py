import networkx as nx
import numpy as np

import warnings
warnings.filterwarnings("ignore")

def gen_graph(edm):
    G = nx.Graph()
    for i in range(edm.shape[0]):
        for j in range(edm.shape[1]):
            G.add_weighted_edges_from([(i,j,edm[i][j])])
    return G

def gen_sparse_graph(edm):
    max_value = np.ndarray.max(edm)
    G = nx.Graph()
    for i in range(edm.shape[0]):
        for j in range(edm.shape[1]):
            if not edm[i,j] == max_value:
                G.add_weighted_edges_from([(i,j,edm[i][j])])
    return G