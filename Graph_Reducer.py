import networkx as nx
import Graph_Analyser as ga
import numpy as np

def Reduce_Graph(edm,removed_nodes_indices):
    graph_mod = ga.gen_sparse_graph(edm)
    ordered_nodes = Order_Nodes_By_Degree(graph_mod)
    high_degree_nodes = Get_Highest_Degree_Nodes(ordered_nodes,edm)
    spanning_nodes, paths = Generate_Shortest_Path_Tree(graph_mod, high_degree_nodes, ordered_nodes)
    return Plot_Shortest_Network(graph_mod,edm,paths,removed_nodes_indices), Generate_Entropy_Matrix(paths)

def Order_Nodes_By_Degree(graph):
    nodes_degrees = []
    for node in graph.nodes:
        nodes_degrees.append((node,graph.degree(node)))
    return nodes_degrees

def Get_Highest_Degree_Nodes(nodes_degrees,edm):
    max_degree_nodes = []
    sorted_node_by_degree = sorted(nodes_degrees, key=lambda x: x[1])
    max_value = sorted_node_by_degree[len(sorted_node_by_degree)-1][1]
    for i in range(len(nodes_degrees)):        
        if nodes_degrees[i][1] == max_value:
            max_degree_nodes.append(nodes_degrees[i][0])
    return max_degree_nodes

def Generate_Shortest_Path_Tree(graph_mod, max_degree_nodes, ordered_nodes):
    spanning_nodes = []
    paths = []
    for node in range(len(ordered_nodes)):
        #print("Node:", ordered_nodes[node][0])
        #if ordered_nodes[node][0] in spanning_nodes:
            #print("Node Covered")
            #continue
        for high_deg_node in range(len(max_degree_nodes)):
            #print("Checking against high node:", max_degree_nodes[high_deg_node])
            path = ga.shortest_path(graph_mod, ordered_nodes[node][0], max_degree_nodes[high_deg_node])
            paths.append(path)
            for hop in range(len(path)):
                if not path[hop] in spanning_nodes:
                    spanning_nodes.append(path[hop])
            #print(path)
        #print("Spanning Nodes:",spanning_nodes)        
    return spanning_nodes, paths

def Plot_Shortest_Network(graph,edm,paths,removed_nodes_indices):
    min_graph = nx.graph.Graph()
    min_graph.add_nodes_from(nodes_for_adding=np.arange(edm.shape[0]))
    for path in range(len(paths)):
        Plot_Shortest_Path(min_graph,edm,paths[path])
    for node in removed_nodes_indices:
        min_graph.remove_node(node)
    return min_graph

def Plot_Shortest_Path(graph,edm,path):
    if len(path) < 2 :
        return
    start_node = path[0]
    end_node = None
    for node in range(len(path)-1):
        end_node = path[node+1]
        graph.add_path([start_node,end_node],weight=edm[start_node][end_node])
        start_node = end_node
        
def Generate_Entropy_Matrix(paths):
    entropy_matrix = []
    npa = []    
    for i in range(len(paths)):
        for j in range(len(paths[i])):
            npa.append(paths[i][j])
    unique, entropy_matrix = np.unique(npa,return_counts=True)
    
    return entropy_matrix