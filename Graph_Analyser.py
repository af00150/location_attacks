import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

import warnings
warnings.filterwarnings("ignore")

def add_cluster_to_graph(graph, cluster, edge_colour, node_colour):
    #add weighted edge to graph with cluster colour    
    for i in len(cluster):
        graph.add_weighted_edges_from(cluster[i],edge_colour=edge_colour,node_colour=node_colour) #set edge colour
    return graph

def gen_graph(edm):
    G = nx.Graph()
    for i in range(edm.shape[0]):
        for j in range(edm.shape[1]):
            G.add_weighted_edges_from([(i,j,edm[i][j])])
    return G

def gen_sparse_graph(edm):
    max_value = np.ndarray.max(edm)
    G = nx.Graph()
            
    for i in range(edm.shape[0]):
        for j in range(edm.shape[1]):
            if not edm[i,j] == max_value:
                G.add_weighted_edges_from([(i,j,edm[i][j])])              
    return G

def gen_graph_nodes(nodes):
    G = nx.Graph()
    for node in range(nodes):
        G.add_node(node)    
    return G

def is_path(graph, source, dest):
    return nx.has_path(graph, source, dest)

def shortest_path_length(graph, source, dest):
    return nx.astar_path_length(graph, source, dest)

def shortest_path(graph,source,dest):
    return nx.astar_path(graph,source,dest)

def is_connected_graph(graph):
    return nx.is_connected(graph)

def generate_cluster(graph, source, hops):
    cluster = graph.single_source_shortest_path_length(graph, source, [hops])
    return cluster

def Get_Graph_Entropy(cluster_counts):
    ordered_entropy = []
    for i in range(len(cluster_counts)):
        ordered_entropy.append((i,cluster_counts[i]))
        
    return ordered_entropy

def Get_Graph_Inverse_Entropy(cluster_counts):
    ordered_entropy = []
    max_ent = np.amax(cluster_counts)
    for i in range(len(cluster_counts)):
        ordered_entropy.append((i,max_ent - cluster_counts[i]))
        
    return ordered_entropy