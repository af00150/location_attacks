import networkx as nx
import numpy as np
import Graph_Analyser as ga

import warnings
warnings.filterwarnings("ignore")

def generate_MST(graph):
    return nx.minimum_spanning_tree(graph)

def generate_clusters(undirected_graph, cluster_size):
    min_cluster_size_cycles = []
    
    #convert the grapg to a directed graph
    directed_graph = undirected_graph.to_directed()
    #anaylyse all cycles O(n^2)
    cycles = list(nx.simple_cycles(directed_graph))

    #iterate over all cycles found
    for i in range(len(cycles)):
        #only use the cycles that have correct hop count
        if len(cycles[i]) == cluster_size:
            #set intial plurality state to False
            plurality = False
            #generate the pluraility matrix against current cycle
            plurality_matrix = np.isin(min_cluster_size_cycles,cycles[i])
            #iterate the matrix to find plurals of cycle
            for j in range(len(plurality_matrix)):
                #if this is true then there is a plural of a cycle
                if plurality_matrix[j].all():                    
                    plurality = True
                    #can stop checking here for more plurals
                    #break
            if not plurality:
                #add the unqiue array to the list
                min_cluster_size_cycles.append(cycles[i])

    print("The clusters formed that are of size {}:".format(cluster_size))
    
    print("Nodes that have no cluster:")
    #flattens the array to get all unqiue values
    uniquie_nodes = np.unique(min_cluster_size_cycles)
    
    #iterate nodes in the graph that dont have affiliation with a cluster
    for i in range(len(directed_graph.nodes)):
        if i not in uniquie_nodes :
            print("\tNode {}:".format(i))
            #get adjacent nodes to form pair cluster
            nodes = [n for n in directed_graph.neighbors(i)]
            for j in range(len(nodes)):
                print("\t\tCluster formed [{},{}].".format(i,nodes[j]))
                #add new cluster to the collection of min clusters
                min_cluster_size_cycles.append([i,nodes[j]])  
            
    return directed_graph, min_cluster_size_cycles

def pair_clustering(edm):
    
    #set an initial colour for the edges (needs implementing)
    #colour = 'red'
    #generate graph from EDM (no edges)
    graph = ga.gen_graph(edm)   
    #empty set of clusters
    clusters = []
    #get EDM "infinite state" (largest value)
    infinity = edm.argmax()   
    #for loop
    for i in range(len(edm.flatten())) :
        #get smallest value from EDM
        value = edm.flatten()[np.argmin(edm)]
        #check that the value is smaller than the inifnite state value (has real Euclidian Distance to node)       
        if value >= infinity :
            #this means that there are no more nodes to be checked
            break
        
        #find the index of the value    
        (j, k) = np.unravel_index(np.argmin(edm),b.shape)    
        #create the pair cluster
        clusters.append([j,[k,value]])
              
        #set the value of the completed edge to infinity
        #stops repeated checks on this value
        edm[j][k] = infinity
        
    #clusters need to be allocated a colour for the edges and nodes    
    #graph = add_cluster_to_graph(graph,clusters,edge_colour,node_colour)
        
    return graph, clusters
    
#used to cluster with a fixed number of nodes per cluster
def N_Clustering(edm, N):
    #generate EDM from graph
    graph = ga.gen_graph(edm)
    #Stores the generated clusters with nodes in 2D list
    #format [source,[dest,weight],[dest,weight],...]
    clusters = []    
    #get EDM max value
    infinity = edm.argmax()
    #edm dimension 
    size = edm.shape[0]
    #iterate the EDM
    for node in size:
        pairs = 0
        local_cluster = []        
        #add the node to the current cluster
        local_cluster.append(node)
        while pairs < N :
            #find argmin of this nodes array
            min_pair = np.argmin(edm[node])
            #check if the node is the one that is currently being checked
            if min_pair == node:
                continue
            #if not
            #get weight of the edge between
            value = edm.flatten()[np.argmin(edm)]
            #get min node indecies
            (j, k) = np.unravel_index(np.argmin(edm),b.shape)
            #set distance to infinity to stop checking same node
            edm[j,k] = infinity
            #add the new nearest node to the list.
            local_cluster.append([min_pair,value])
            
            #on successful pairing
            pairs += 1
        #add the cluster to the list of clusters
        clusters.append(local_cluster)
        #reset value for next node
        pairs = 0 
        
    #clusters need to be allocated a colour for the edges and nodes    
    #graph = add_cluster_to_graph(graph,clusters,edge_colour,node_colour)
        
    return graph, clusters

#used to cluster with a random number of nodes per cluster
def N_Clustering_Random(edm):
    import random
    #generate new random int within 0 and EDM width
    N = randint(0,edm.shape[0])
    #run N_Clustering on random N
    graph, clusters = N_Clustering(edm,N)
    
    return graph, clusters

#set nodes can be selected to initialize the clusters
#ths is useful for exploring the same resulting cluster graph
def P_Clustering(edm, cluster_nodes, min_overlaps):
    #generate graph from EDM
    graph = ga.gen_graph(edm)
    #empty set of clusters
    clusters = []
    #get max value in EDM
    infinity = edm.argmax() 
    value = 0
    #iterate the selected clusters
    for i in len(cluster_nodes):  
        local_cluster = []
        for j in min_overlaps:
            #get the current itterations node
            node = cluster_nodes[i]
            #find clsotest node to initial node cluster
            (j, k) = np.unravel_index(np.argmin(edm),b.shape)
            #get the edge value
            value = edm.flatten()[np.argmin(edm)]
            #add the new nearest node to the current cluster
            local_cluster = [i,[j,value]]  
            #set the value selected from the EDM to "infinity"
            edm[j,k] = infinity
                
    #clusters need to be allocated a colour for the edges and nodes    
    #graph = add_cluster_to_graph(graph,clusters,edge_colour,node_colour)
    
    return graph, clusters


#randmoise the nodes that are selected to form the initial node set
#this is useful when looking to explore the space and find a more optimal setup
def P_Clustering_Random(edm,number_of_clusters,min_overlaps):
    import random
    
    #generate "node_of_clusters" node points
    node_clusters = []
    
    for i in number_of_clusters:
        #generate new number from 0 to max index of the EDM
        node = random.randint(0,edm.shape[0])
        #check if the node is in the current set of selected nodes
        if node not in node_clusters:
            #add the node if it isn't in the generated set
            node_clusters.append(node)
        else:
            #decriment the current itteration to all for 
            #another node to be generated at random
            i-=1
    
    #call P-Clustering
    graph, clusters = P_Clustering(edm, node_clusters, min_overlaps)
    #clusters need to be allocated a colour for the edges and nodes    
    #graph = add_cluster_to_graph(graph,clusters,edge_colour,node_colour)
    return graph, clusters

def Cyclic_Clustering_With_Entropy_Analysis(edm, min_clustering, cluster_size):

    cluster_count = [0]*edm.shape[0]
    clusters = []
    graph = nx.Graph()

    for i in range(edm.shape[0]*3):
        j, k = np.unravel_index(np.argmin(edm),edm.shape)
        #get the edge value
        value = edm.flatten()[np.argmin(edm)]   
        if value == 99:
            break
        graph.add_weighted_edges_from([(j,k,value)])
 
        edm[j][k] = 99    
        edm[k][j] = 99
        
        cluster_count[j]+=1
        cluster_count[k]+=1
        
        if np.amin(cluster_count) == min_clustering:
            break

    graph, clusters = generate_clusters(graph, cluster_size)
    entropy_mask = ga.Get_Graph_Entropy(cluster_count)

    return graph, clusters, entropy_mask

def Cyclic_Clustering(undirected_graph, cluster_size):
    min_cluster_size_cycles = []
    
    #convert the grapg to a directed graph
    directed_graph = undirected_graph.to_directed()
    #anaylyse all cycles O(n^2)
    cycles = list(nx.simple_cycles(directed_graph))

    #iterate over all cycles found
    for i in range(len(cycles)):
        #only use the cycles that have correct hop count
        if len(cycles[i]) == cluster_size:
            #set intial plurality state to False
            plurality = False
            #generate the pluraility matrix against current cycle
            plurality_matrix = np.isin(min_cluster_size_cycles,cycles[i])
            #iterate the matrix to find plurals of cycle
            for j in range(len(plurality_matrix)):
                #if this is true then there is a plural of a cycle
                if plurality_matrix[j].all():                    
                    plurality = True
                    #can stop checking here for more plurals
                    #break
            if not plurality:
                #add the unqiue array to the list
                min_cluster_size_cycles.append(cycles[i])

    print("The clusters formed that are of size {}:".format(cluster_size))
    
    print("Nodes that have no cluster:")
    #flattens the array to get all unqiue values
    uniquie_nodes = np.unique(min_cluster_size_cycles)
    
    #iterate nodes in the graph that dont have affiliation with a cluster
    for i in range(len(directed_graph.nodes)):
        if i not in uniquie_nodes :
            print("\tNode {}:".format(i))
            #get adjacent nodes to form pair cluster
            nodes = [n for n in directed_graph.neighbors(i)]
            for j in range(len(nodes)):
                print("\t\tCluster formed [{},{}].".format(i,nodes[j]))
                #add new cluster to the collection of min clusters
                min_cluster_size_cycles.append([i,nodes[j]])  
            
    return directed_graph, min_cluster_size_cycles

def Entropy_Clustering(entropy_matrix,graph,growth_rate):
    #initialize the array values to 0s
    entropy_values = entropy_matrix
    #initialize the entropy values to 50% original value
    E = entropy_matrix
    #print(E)
    graph_sets = []
    #get the max edge weight in the graph
    max_edge_length = sorted(graph.edges.data('weight'), key=lambda x:x[2])[len(graph.edges)-1][2]
    current_edge_length = 0
    #get all edges in the graph
    edges = list(graph.edges.data('weight'))
    iteration = 0
    #loop while the max entropy of the nodes is less than then entropy
    #threashold of the graph (max edge weight)
    while current_edge_length <= max_edge_length:

        iteration_set = []

        #iterate over all nodes radii
        for node in range(len(E)):
            node_edge_set = []

            #iterate over all the edges
            for edge in range(len(edges)):
                #check that that edge srouce is the current node
                #check that the edge weight is <= current nodes radius
                if (edges[edge][0] == node) and (edges[edge][2] <= E[node]):               
                    #create the new edge
                    new_edge = [edges[edge][0],edges[edge][1],edges[edge][2]]
                    node_edge_set.append(new_edge)
                    #print("\t"+str(new_edge))
            #add the nodes edges to the iteratation set
            if len(node_edge_set) > 0:
                iteration_set.append(node_edge_set)
            iteration += 1
        iteration = 0
        #add the iteration set to the line in table for this iteration
        if len(iteration_set) > 0:
            graph_sets.append(iteration_set)
        #increment the entropy values by the growth_rate
        incriment_norm_array(E, growth_rate)
        E += growth_rate
        #print(E)
        #update the current min entropy value
        current_edge_length = np.amin(E)

    return graph_sets[iteration-1]

def incriment_norm_array(norm_array,inc_value):
    arr = np.array(norm_array)
    return [x+(inc_value*x) for x in arr]

#from here the clusters can be generate by looking at the size of the cluster
#checks need to be added for non-intersection clusters
#if clusters dont intersect over the entire graph then these can
#be considered as seperate network and this isnt the case
#this can happen when the cluster size is too small

def compute_final_clusters_new(entropy_cluster_size,cluster_sets):
    grouped_clusters = np.zeros(shape=(1,10))
    
    for cluster in range(len(cluster_sets)):
        grouped_clusters[len(cluster_sets[cluster])].append(cluster_sets[cluster])
    return final_clusters
            

def compute_final_clusters(cluster_size, clusters):
    #iterates the node cluster sets and find the cluster size required
    final_clusters = []
    all_clusters = []
    for row in range(len(clusters)):
        #print("Row {}".format(row))
        for node in range(len(clusters[0])):
            #print("\tNode {}".format(node))
            current_cluster_set = clusters[row][node] 
            all_clusters.append(current_cluster_set)
            if len(current_cluster_set) == cluster_size - 1:
                #print("Has correct numbr of clustered nodes")
                #print (current_cluster_set)

                if current_cluster_set not in final_clusters:
                    final_clusters.append(current_cluster_set)
    #print()
    print("Clusters")
    for i in final_clusters:
        print(i)
    return final_clusters, all_clusters

#Check for intersection between all clusters
#Notify as to the cluster that doesnt have an intersection

def check_intersection_instances(edm,clusters,instances):
    non_intersecting_node = []
    print("No multi-intersections at nodes:")
    for i in range(edm.shape[0]):
        intersections = 0
        for cluster in range(len(clusters)):
            intersections += len(np.intersect1d([i],[clusters[cluster]]))
            #if len(np.intersect1d([i],[clusters[cluster]])) > 0:
            #    intersections +=1
        if intersections < instances:
            non_intersecting_node.append(i)
            #print(i)
    return non_intersecting_node

#check that all nodes are covered by the selection
def check_cluster_coverage(edm,clusters):
    from functools import reduce
    print("No intersections found for nodes:")
    values = []
    for i in range(edm.shape[0]):
        if len(reduce(np.intersect1d, [i],clusters)) == 0:
            print(i)
            values.append(i)
    return values

#find a cluster that is appropriate
#user must indicate a larger or smaller cluster to be selected 
#or the cluster can leave blank and have a random cluster selected
#if no clusters are avaliable then the user should reconfigure the network parameters

def test_coverage(edm,clusters,all_clusters):
    missing_nodes = check_cluster_coverage(edm,clusters)
    print("Avaliable clusters to choose from:")
    avaliable_choices = []
    for cluster in range(len(all_clusters)):
        if len(np.intersect1d(missing_nodes,all_clusters[cluster])):
            if all_clusters[cluster] not in avaliable_choices:
                avaliable_choices.append(all_clusters[cluster])
    print(avaliable_choices)
    return avaliable_choices        