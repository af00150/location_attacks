def dif_clusters(clusterA, clusterB, node_count):
    import numpy as np
    attacked_node = None
    print("Started")
    #the intersections found in the original cluster must match exactly the instersection found in the attacked cluster
    #if they don't match then an attack has occured.
    
    for i in range(len(clusterA)):    
        print("Comapring clusters:",i)
        #for the original cluster
        trimmed_original_clustersA = [np.unique(np.delete(clust,2,0)) for clust in clusterA[i]]

        arr = np.zeros((10,1))
        for clus in trimmed_original_clustersA:
            for node in clus:
                #print(node)
                arr[int(node)]+=1
        #apply one hot encoding
        arr = (arr > 0).astype(int)
        #print(arr)

        #for the attacked cluster
        trimmed_original_clustersB = [np.unique(np.delete(clust,2,0)) for clust in clusterB[i]]

        arr2 = np.zeros((10,1))
        for clus in trimmed_original_clustersB:
            for node in clus:
                #print(node)
                arr2[int(node)]+=1    
        #apply boolean encoding based on existance
        arr2 = (arr2 > 0).astype(int)
        #print(arr2)
        print(arr == arr2)
        print("Cluster",i,"match? ",np.all(arr == arr2))
    
    #identify where the differences are for the results
    print("Compelted")
    
    return attacked_node